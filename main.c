#include <stdio.h>
#include <stdlib.h>

int fibonacci(int n) {
    if (n < 0)
        return -1;
    if (n == 0)
        return 0;
    else if (n == 1)
        return 1;
    else
        return fibonacci(n - 1) + fibonacci(n - 2);
}

int main(int argc, const char *argv[]) {
    FILE *file = fopen(argv[1], "r");
    char line[1024];
    while (fgets(line, 1024, file)) {
        printf("%d\n", fibonacci(atoi(line)));
    }
    return 0;
}